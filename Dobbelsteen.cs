﻿using System;

namespace Oef_23._1
{
    internal class Dobbelsteen
    {
        private int _waarde;
        private int _aantalZijden;
        private Random _willGetal;

        public Dobbelsteen()
        {
            AantalZijden = 6;
        }

        public Dobbelsteen(int aantalZijden, Random r)
        {
            AantalZijden = aantalZijden;
            WillGetal = r;
        }

        public Dobbelsteen(Random r)
        {
            AantalZijden = 6;
            WillGetal = r;
        }

        public int Waarde
        {
            get { return _waarde; }
            set { _waarde = value; }
        }

        public int AantalZijden
        {
            get { return _aantalZijden; }
            set { _aantalZijden = value; }
        }


        public Random WillGetal
        {
            get { return _willGetal; }
            set { _willGetal = value; }
        }

        public void Roll()
        {
            WillGetal = new Random();
            Waarde = WillGetal.Next(1, AantalZijden + 1);
        }

    }
}
