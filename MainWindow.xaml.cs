﻿using System;
using System.Windows;

namespace Oef_23._1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Dobbelsteen dobbelsteen = new Dobbelsteen();

        }

        private void btnGooien_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();

            Dobbelsteen dobbelsteen1 = new Dobbelsteen(4, random);
            dobbelsteen1.Roll();
            Dobbelsteen dobbelsteen2 = new Dobbelsteen();
            dobbelsteen2.Roll();

            lblBlauw.Content += dobbelsteen1.Waarde.ToString();
            lblRood.Content += dobbelsteen2.Waarde.ToString();


        }
    }
}
